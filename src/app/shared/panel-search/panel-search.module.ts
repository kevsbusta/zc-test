import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PanelSearchComponent} from './panel-search.component';
import {PanelSearchFieldDirective, PanelSearchDropdownDirective, PanelSearchButtonDirective} from './panel-search.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PanelSearchComponent,
    PanelSearchFieldDirective,
    PanelSearchDropdownDirective,
    PanelSearchButtonDirective
  ],
  exports: [
    PanelSearchComponent,
    PanelSearchFieldDirective,
    PanelSearchDropdownDirective,
    PanelSearchButtonDirective
  ]
})
export class PanelSearchModule { }
