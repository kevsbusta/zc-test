import {Directive} from '@angular/core';

@Directive({selector: 'app-panel-search-field'})
export class PanelSearchFieldDirective {
  constructor() {
  }
}

@Directive({selector: 'app-panel-search-dropdown'})
export class PanelSearchDropdownDirective {
  constructor() {
  }
}

@Directive({selector: 'app-panel-search-button'})
export class PanelSearchButtonDirective {
  constructor() {
  }
}