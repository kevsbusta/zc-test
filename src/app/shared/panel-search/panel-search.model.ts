export interface PanelSearchItem {
    skill?: string;
    location?: string;
}