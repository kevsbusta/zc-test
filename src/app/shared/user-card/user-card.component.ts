import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { User } from '../../recommendation/main/user-mock-data';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserCardComponent implements OnInit {

  @Input()
  public user: User;

  constructor() { }

  ngOnInit() {
  }

}
