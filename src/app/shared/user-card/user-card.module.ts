import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import { UserCardComponent } from './user-card.component';

@NgModule({
  imports: [
    CommonModule,
    CardModule,
    ButtonModule
  ],
  declarations: [
    UserCardComponent
  ],
  exports: [
    UserCardComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserCardModule { }
