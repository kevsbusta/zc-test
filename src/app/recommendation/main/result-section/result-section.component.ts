import { Component, OnInit, Input } from '@angular/core';
import { User } from '../user-mock-data';

@Component({
  selector: 'app-result-section',
  templateUrl: './result-section.component.html',
  styleUrls: ['./result-section.component.css']
})
export class ResultSectionComponent implements OnInit {

  @Input()
  public data: User[];

  constructor() { }

  ngOnInit() {
    console.log(this.data);
  }

}
