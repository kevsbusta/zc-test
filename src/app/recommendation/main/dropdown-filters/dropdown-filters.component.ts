import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-dropdown-filters',
  templateUrl: './dropdown-filters.component.html',
  styleUrls: ['./dropdown-filters.component.css']
})
export class DropdownFiltersComponent implements OnInit {

  @Output() sortBy: EventEmitter<any> = new EventEmitter<any>();
  @Output() filterBy: EventEmitter<any> = new EventEmitter<any>();

  sortByOptions = [];
  filterByOptions = [];
  filterByModel: any;
  sortByModel: any;

  constructor() {
    this.sortByOptions = [
      {label: 'Name Ascending', value: {icon: 'asc', value: 'Name Ascending'}},
      {label: 'Name Descending', value: {icon: 'desc', value: 'Name Descending'}},
      {label: 'Location Ascending', value: {icon: 'asc', value: 'Location Ascending'}},
      {label: 'Location Descending', value: {icon: 'desc', value: 'Location Descending'}},
      {label: 'Crowd Ascending', value: {icon: 'asc', value: 'Crowd Ascending'}},
      {label: 'Crowd Descending', value: {icon: 'desc', value: 'Crowd Descending'}},
      {label: 'Rating Ascending', value: {icon: 'asc', value: 'Rating Ascending'}},
      {label: 'Rating Descending', value: {icon: 'desc', value: 'Rating Descending'}},
      {label: 'Reputation Ascending', value: {icon: 'asc', value: 'Reputation Ascending'}},
      {label: 'Reputation Descending', value: {icon: 'desc', value: 'Reputation Descending'}},
    ];
    this.filterByOptions = [
      {label: 'All Results', value: 'ALL'},
      {label: 'My Crowd', value: 'CROWD'},
    ];
  }

  ngOnInit() {
  }

  filterByChange(event) {
    this.filterBy.emit(event);
  }

  sortByChange(event) {
    this.sortBy.emit(event.value);
  }

}
