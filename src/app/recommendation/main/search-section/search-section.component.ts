import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { PanelSearchItem } from '../../../shared/panel-search/panel-search.model';

@Component({
  selector: 'app-search-section',
  templateUrl: './search-section.component.html',
  styleUrls: ['./search-section.component.css']
})
export class SearchSectionComponent implements OnInit {

  @Output()
  public onSubmit: EventEmitter<PanelSearchItem> = new EventEmitter<PanelSearchItem>();

  public searchItem: PanelSearchItem = {};

  locations: SelectItem[];

  constructor() {

    this.locations = [
      {label: 'Manila', value: 'Manila'},
      {label: 'Melbourne', value: 'Melbourne'}
  ];
 }

  ngOnInit() {
  }

  submit() {
    this.onSubmit.emit(this.searchItem);
  }
}
