import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { PanelSearchItem } from '../../shared/panel-search/panel-search.model';
import {userMockData, User} from './user-mock-data';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {

  totalRecordsCount: number;
  public data: User[];
  public searchItem: PanelSearchItem = {};
  public len = userMockData.length;
  public rows = 4;
  public first = 0;
  public sortByModel: string;
  public filterByModel: string;

  constructor() {
  }

  ngOnInit() {
    this.data = userMockData;
    this.len = this.data.length;
    this.filter();
  }

  submit(searchItem: PanelSearchItem) {
    this.searchItem = searchItem;
    this.filter();
  }

  filter() {
    const userMock = userMockData;
    this.data = userMock.filter((d) => {
      const rgxSkill = new RegExp(this.searchItem.skill, 'ig');
      const rgxLocation = new RegExp(this.searchItem.location, 'ig');
      const findBySkill = d.skills.findIndex(skill => rgxSkill.test(skill)) >= 0;
      const findByCountry = rgxLocation.test(d.city);

      let crowd = true;
      if (this.filterByModel === 'CROWD') {
        crowd = d.connected;
      }

      return findBySkill && findByCountry && crowd;
    });

    this.len = this.data.length;
    this.data = this.data.slice(this.first, this.first + this.rows);
  }

  loadData(event: any) {
    this.rows = event.rows;
    this.first = event.first;
    this.filter();
  }

  sortBy(event: any) {
    this.sortByModel = event;
  }

  filterBy(event: any) {
    this.filterByModel = event;
    this.filter();
  }
}
