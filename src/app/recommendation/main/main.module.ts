import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MainComponent} from './main.component';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {ButtonModule} from 'primeng/button';
import {PanelSearchModule} from '../../shared/panel-search/panel-search.module';
import { SearchSectionComponent } from './search-section/search-section.component';
import { ResultSectionComponent } from './result-section/result-section.component';
import { UserCardModule } from '../../shared/user-card/user-card.module';
import {PaginatorModule} from 'primeng/paginator';
import { DropdownFiltersComponent } from './dropdown-filters/dropdown-filters.component';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    DropdownModule,
    ButtonModule,
    PanelSearchModule,
    FormsModule,
    UserCardModule,
    PaginatorModule
  ],
  declarations: [
    MainComponent,
    SearchSectionComponent,
    ResultSectionComponent,
    DropdownFiltersComponent
  ],
  exports: [
    MainComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainModule { }
