

export const sampleConnections: User[] = [
    {
        id: 1,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila'
    },
    {
        id: 2,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila'
    },
    {
        id: 3,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila'
    },
    {
        id: 4,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila'
    },
    {
        id: 5,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila'
    }
];

export const userMockData: User[] = [
    {
        id: 1,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila',
        connections: this.sampleConnections,
        followers: this.sampleConnections,
        connected: true,
        pending: false
    },
    {
        id: 2,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila',
      connected: true,
      pending: true
    },
    {
        id: 3,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila',
      connected: true,
      pending: false
    },
    {
        id: 4,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Angular'],
        country: 'Philippines',
        city: 'Melbourne',
      connected: false,
      pending: false
    },
    {
        id: 5,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Melbourne',
      connected: false,
      pending: false
    },
    {
        id: 6,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila',
        connections: this.sampleConnections,
        followers: this.sampleConnections,
      connected: false,
      pending: false
    },
    {
        id: 7,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila',
      connected: false,
      pending: false
    },
    {
        id: 8,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila',
      connected: true,
      pending: true
    },
    {
        id: 9,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila',
      connected: true,
      pending: true
    },
    {
        id: 10,
        image: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
        firstName: 'Kevin',
        lastName: 'Bustamante',
        skills: ['Frontend Developer', 'C# Dev', 'Rails', 'Angular'],
        country: 'Philippines',
        city: 'Manila',
      connected: false,
      pending: true
    }
];

export interface User {
    id: number;
    image: string;
    firstName: string;
    lastName: string;
    skills: string[];
    country: string;
    city: string;
    connections?: User[];
    followers?: User[];
    connected?: boolean; // Indicator if you are friend or connected to this user
    pending?: boolean;
}
